document.addEventListener("DOMContentLoaded", function () {
        
  if ($(window).width() > 767) {

    Array.from(document.querySelectorAll('#timetable-slider')).forEach(function (el, i) {
      let carousel = tns({
        container: el,
        autoWidth: true,
        speed: 500,
        gutter: 0,
        controls: true,
        controlsText: ['<svg width="25" height="44" viewBox="0 0 25 44" fill="none">' +
        '<path d="M23 42L3 22L23 2" stroke="currentColor" stroke-width="3"/>' +
        '</svg>', '<svg width="25" height="44" viewBox="0 0 25 44" fill="none">' +
        '<path d="M2 2L22 22L2 42" stroke="currentColor" stroke-width="3"/>\n' +
        '</svg>'],
        nav: false,
        mouseDrag: false,
        swipeAngle: 25,
        touch: true,
        loop: false,
        rewind: false,
        responsive: {
          992: {
            mouseDrag: true
          }
        },
        onInit: function (slider) {}

      });

    });

    $('.timetable__slider').each(function() {

      for (var i = 0; i < 2; i++) {
        var cardHeightsArray = [];

        $(this).find('.timetable__col').each(function() {

          var cardHeight = $(this).find('.card').eq(i).not('.card--big').outerHeight();
          cardHeightsArray.push(cardHeight);
        });

        cardHeightsArray = cardHeightsArray.filter(function (el) {
          return el != null;
        });

        var cardMaxHeight = Math.max.apply(Math, cardHeightsArray);

        $(this).find('.timetable__col').each(function() {
          $(this).find('.card').eq(i).css('min-height', cardMaxHeight);
        });
      }
    });
  }
        
  else {

    $('.timetable__slider-wrap').each(function() {
      
      var rowSlider = $(this).find('.timetable__slider-row');

      for (var i = 0; i < 2; i++) {

        $(this).find('.timetable__col').each(function() {
          $(this).find('.card').eq(i).clone().appendTo(rowSlider.eq(i));
        });
      }
    });

    Array.from(document.querySelectorAll('.timetable__slider-row')).forEach(function (el, i) {
      let carouselMobile = tns({
        container: el,
        speed: 500,
        gutter: 0,
        items: 1,
        controls: true,
        controlsText: ['<svg width="17" height="28" viewBox="0 0 17 28" fill="none">' +
        '<path d="M15 26L3 14L15 2" stroke="#737F89" stroke-width="3"/>' +
        '</svg>', '<svg width="17" height="28" viewBox="0 0 17 28" fill="none">' +
        '<path d="M2 2L14 14L2 26" stroke="#737F89" stroke-width="3"/>' +
        '</svg>'],
        nav: false,
        mouseDrag: false,
        swipeAngle: 25,
        touch: true,
        loop: false,
        rewind: false,
        responsive: {
          992: {
            mouseDrag: true
          }
        },
        onInit: function (slider) {}

      });

    });
  }
});
