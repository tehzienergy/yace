var $status = $('.solo-slider__counter');
var $slickElement = $('.solo-slider__content');

$slickElement.slick({
  adaptiveHeight: true,
  infinite: false
}).on('init reInit afterChange', function(event, slick, currentSlide, nextSlide){
    var i = (currentSlide ? currentSlide : 0) + 1;
    $status.text(i + '/' + slick.slideCount);
});
