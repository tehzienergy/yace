document.addEventListener("DOMContentLoaded", function() {
  Array.from(document.querySelectorAll('.info__links-slider')).forEach(function(el,i) {

    var carousel = tns({
      container: el,
      autoWidth: true,
      speed: 500,
      controls: true,
      controlsText: ['<svg width="25" height="44" viewBox="0 0 25 44" fill="none">' +
      '<path d="M23 42L3 22L23 2" stroke="currentColor" stroke-width="3"/>' +
      '</svg>','<svg width="25" height="44" viewBox="0 0 25 44" fill="none">' +
      '<path d="M2 2L22 22L2 42" stroke="currentColor" stroke-width="3"/>\n' +
      '</svg>'],
      nav: false,
      mouseDrag: true,
      swipeAngle: 25,
      touch: true,
      loop: false,
      rewind: false
    })
    
    
    
    var customizedFunction = function (info, eventName) {
      
      if (info.index == (info.slideCount - info.items)) {
        $('.info__links-slider').addClass('is-right');
      }
      
      else {
        $('.info__links-slider').removeClass('is-right');
      }
    }

  })
});
