function pswToggleVisible(b){
  let btn = $(b);
  let t = 'text';
  let input = $(btn).parents('.form-group').find('input');
  btn.toggleClass('is-active');
  
  if (btn.hasClass('is-active')){
    t = 'password';
  }
  input.attr('type',t);
}

jQuery(document).ready(function($) {
  
  $('[data-target="#badge"]').on('click', function (e) {
    var cabinetBadgeImg = $('.cabinet__badge-img')
    if ($(this).attr('aria-expanded') === 'false') {
      cabinetBadgeImg.addClass('is-hidden')
    } else {
      cabinetBadgeImg.removeClass('is-hidden')
    }
  })

  $('.js-scroll-to').on('click', function(e) {
    e.preventDefault()
    var headerHeight = $('.header').height()
    $('html, body').animate({
      scrollTop: $( $(this).attr('href') ).offset().top - headerHeight
    }, 1000)
  })
  
})
