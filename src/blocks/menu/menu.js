if ($(window).width() < 992) {
  $('.menu__sublist-header').click(function(e) {
    e.preventDefault();
    $(this).closest('.menu__sublist-wrapper').toggleClass('is-active');
  })
}

else {
  $('.menu__sublist-wrapper').removeClass('is-active');
}
