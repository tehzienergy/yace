jQuery(document).ready(function($) {
  var btnGotop = $('.btn-gotop')
  if (btnGotop.length) {

    btnGotop.on('click', function (e) {
      e.preventDefault()
      $('body, html').animate({
        scrollTop: 0
      }, 1000);
    })
    
  }
})
