jQuery(document).ready(function($) {
  
  var formControl =  $('.form-control')
  var pswToggle = $('.form-group__psw-toggle')
  
  if (formControl.length) {
    formControl.each(function () {
      if ($(this).val() !== '') {
        $(this).addClass('is-active')
      } else {
        $(this).removeClass('is-active')
      }
    })

    formControl.on('blur', function (e) {
      var value = $(this).val()
      if (value !== '') {
        $(this).addClass('is-active')
      } else {
        $(this).removeClass('is-active')
      }
    })
  }
  
  if (pswToggle.length) {
    pswToggle.on('click', function (e) {
      e.preventDefault()
      var input = $(this).siblings('.form-control')
      if (input.attr('type') === 'password') {
        input.attr('type', 'text')
      } else {
        input.attr('type', 'password')
      }
    })
  }
  
})
