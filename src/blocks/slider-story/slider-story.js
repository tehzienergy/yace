document.addEventListener("DOMContentLoaded", function () {

  Array.from(document.querySelectorAll('.js-slider-story')).forEach(function (el, i) {
    var carousel = tns({
      container: el,
      items: 1,
      speed: 500,
      gutter: 20,
      controls: true,
      controlsText: ['<svg width="25" height="44" viewBox="0 0 25 44" fill="none">' +
      '<path d="M23 42L3 22L23 2" stroke="currentColor" stroke-width="3"/>' +
      '</svg>', '<svg width="25" height="44" viewBox="0 0 25 44" fill="none">' +
      '<path d="M2 2L22 22L2 42" stroke="currentColor" stroke-width="3"/>\n' +
      '</svg>'],
      nav: false,
      mouseDrag: true,
      swipeAngle: 25,
      touch: true,
      loop: false,
      rewind: false,
      responsive: {
        992: {
          items: 3,
          gutter: 60,
        },
        1200: {
          gutter: 90
        }
      }

    })
    
    var customizedFunction = function (info, eventName) {
      // direct access to info object
      if (info.index > 0) {
        $('.slider-story').addClass('is-scrolled');
      }

      else {
        $('.slider-story').removeClass('is-scrolled');
      }
      
      if (info.index == (info.slideCount - info.items)) {
        $('.slider-story').addClass('is-right');
      }
      
      else {
        $('.slider-story').removeClass('is-right');
      }
    }

    // bind function to event
    carousel.events.on('indexChanged', customizedFunction);

  })
})
