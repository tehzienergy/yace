$(window).scroll(function(){
  let header = $('#header');
  let offset = 100;
  if (header.hasClass('header--home')){
    offset = 400;
  }
  if ($(window).scrollTop()>offset){
    $('#header').addClass('is-sticky');
  }else{
    $('#header').removeClass('is-sticky');
  }
});
