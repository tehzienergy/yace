function insertVideo(e,l){
  e.preventDefault();
  let link = $(l);
  let wrap = link.parent();
  let iframe = $('<iframe>',{
    class: 'embed-responsive-item',
    src: link.attr('href'),
    frameborder: '0',
    allow: 'accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope;',
    allowfullscreen: ''
  });
  wrap.append(iframe);
  link.remove();
}
