jQuery(document).ready(function($) {
  if ($('#photoInput').length) {

    var readURL = function(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader()
        reader.onload = function(e) {
          $('.photo__img-wrap img').attr('src', e.target.result)
        }
        reader.readAsDataURL(input.files[0])
      }
    }

    $('#photoInput').on('change', function() {
      readURL(this)
    })
    
  }
})
