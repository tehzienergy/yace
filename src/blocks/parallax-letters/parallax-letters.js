jQuery(document).ready(function ($) {
  // Parallax
  if ($('.js-rellax').length && $(window).width() > 1200) {
    var rellax = new Rellax('.js-rellax', {
      center: false,
    });
  }
})
