$('[data-fancybox]').fancybox({
  touch: false,
  afterShow: function (instance, slide) {

    if ($('.modal__speaker-dscr-content--cut').length) {

      var modalPS = new PerfectScrollbar('.modal__scrollbar');
      
      if ( $('.modal__speaker-dscr-content').height() >= '78' ) {
        
        $('.modal__dscr-controls').show()
        
        let wrapper = document.querySelector(".modal__speaker-dscr-content--cut");
        let options = {
          callback: function (isTruncated) {
            $('.modal__speaker-dscr-content--cut').addClass('modal__speaker-dscr-content--cut--truncated');
          },
          height: 80,
          ellipsis: "\u2026 "
        };
        let dot = new Dotdotdot(wrapper, options);

        $('.modal__dscr-link--more').click(function (e) {
          e.preventDefault();
          dot.API.restore();

          $('.modal__scrollbar').addClass('modal__scrollbar--revealed');

          $('.modal__scrollbar-wrapper').addClass('modal__scrollbar-wrapper--revealed');

          $('.modal__dscr-link').toggleClass('modal__dscr-link--active');

          modalPS.update();
        });

        $('.modal__dscr-link--less').click(function (e) {
          e.preventDefault();
          $('.modal__dscr-link').toggleClass('modal__dscr-link--active');
          $('.modal__scrollbar').removeClass('modal__scrollbar--revealed');
          $('.modal__scrollbar-wrapper').removeClass('modal__scrollbar-wrapper--revealed');
          dot.API.truncate();

          modalPS.update();
        });
      }
      
    }
  }
});

$('.modal__speaker-bookmark').click(function (e) {
  e.preventDefault();
  $(this).toggleClass('modal__speaker-bookmark--active');
});

$('.js-textarea-limited').on("input", function () {
  var maxlength = $(this).attr("maxlength");
  var currentLength = $(this).val().length;

  if (currentLength >= maxlength) {
    $('.js-textarea-counter').addClass('limit');
  } else {
    $('.js-textarea-counter-number').text(currentLength);
    $('.js-textarea-counter').removeClass('limit');
  }
});
