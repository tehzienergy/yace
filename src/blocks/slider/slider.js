document.addEventListener("DOMContentLoaded", function() {
  Array.from(document.querySelectorAll('.js-slider-content-default')).forEach(function(el,i) {

    var sliderContentDefault = tns({
      container: el,
      autoWidth: true,
      speed: 500,
      gutter: 16,
      controls: true,
      controlsText: ['<svg width="25" height="44" viewBox="0 0 25 44" fill="none">' +
      '<path d="M23 42L3 22L23 2" stroke="currentColor" stroke-width="3"/>' +
      '</svg>','<svg width="25" height="44" viewBox="0 0 25 44" fill="none">' +
      '<path d="M2 2L22 22L2 42" stroke="currentColor" stroke-width="3"/>\n' +
      '</svg>'],
      nav: false,
      mouseDrag: true,
      swipeAngle: 25,
      touch: true,
      loop: false,
      rewind: false
    })
    
    var customizedFunction = function (info, eventName) {
      // direct access to info object
      if (info.index > 0) {
        $('.slider').addClass('is-scrolled');
      }
      else {
        $('.slider').removeClass('is-scrolled');
      }
      
      if (info.index == (info.slideCount - info.items)) {
        $('.slider').addClass('is-right');
      }
      
      else {
        $('.slider').removeClass('is-right');
      }
    }

    // bind function to event
    sliderContentDefault.events.on('indexChanged', customizedFunction);

  })

  Array.from(document.querySelectorAll('.js-slider-content-person')).forEach(function(el,i) {

    var sliderContentPerson = tns({
      container: el,
      items: 2,
//      autoWidth: true,
      speed: 500,
      gutter: 20,
      controls: true,
      controlsText: ['<svg width="25" height="44" viewBox="0 0 25 44" fill="none">' +
      '<path d="M23 42L3 22L23 2" stroke="currentColor" stroke-width="3"/>' +
      '</svg>','<svg width="25" height="44" viewBox="0 0 25 44" fill="none">' +
      '<path d="M2 2L22 22L2 42" stroke="currentColor" stroke-width="3"/>\n' +
      '</svg>'],
      nav: false,
      mouseDrag: true,
      swipeAngle: 25,
      touch: true,
      loop: false,
      rewind: false,
      responsive: {
        568: {
          items: 3,
          autoWidth: false
        },
        992: {
          items: 4,
          autoWidth: false
        }
      },
    });
    
    var customizedFunction = function (info, eventName) {
      // direct access to info object
      if (info.index > 0) {
        $('.slider').addClass('is-scrolled');
      }
      else {
        $('.slider').removeClass('is-scrolled');
      }
    }

    // bind function to event
    sliderContentPerson.events.on('indexChanged', customizedFunction);

  })

  Array.from(document.querySelectorAll('.js-slider-content-project')).forEach(function(el,i) {

    var sliderContentProject = tns({
      container: el,
      autoWidth: true,
      speed: 500,
      gutter: 16,
      controls: true,
      controlsText: ['<svg width="25" height="44" viewBox="0 0 25 44" fill="none">' +
      '<path d="M23 42L3 22L23 2" stroke="currentColor" stroke-width="3"/>' +
      '</svg>','<svg width="25" height="44" viewBox="0 0 25 44" fill="none">' +
      '<path d="M2 2L22 22L2 42" stroke="currentColor" stroke-width="3"/>\n' +
      '</svg>'],
      nav: false,
      mouseDrag: true,
      swipeAngle: 25,
      touch: true,
      loop: false,
      rewind: false
    });
    
    var customizedFunction = function (info, eventName) {
      // direct access to info object
      if (info.index > 0) {
        $('.slider').addClass('is-scrolled');
      }
      else {
        $('.slider').removeClass('is-scrolled');
      }
    }

    // bind function to event
    sliderContentProject.events.on('indexChanged', customizedFunction);

  })
});
