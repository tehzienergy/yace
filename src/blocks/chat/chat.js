$(document).ready(function () {
  $('.js-chat-scroll').each(function (i, el) {
    new PerfectScrollbar(el, {
      wheelSpeed: 0.5,
      wheelPropagation: true,
      maxScrollbarLength: 20
    });
  });

  if ($('#chat-scroll').length > 0) {
    let chatScroll = new PerfectScrollbar('#chat-scroll', {
      wheelSpeed: 0.5,
      wheelPropagation: true,
      maxScrollbarLength: 20
    });
  }

});

$('.msg--short').click(function() {
  $('#chat-right').addClass('is-show');
  $('#chat-main .msg').removeClass('is-selected');
  $(user).addClass('is-selected');
});

$('.msg--modal').fancybox({
  baseClass: 'modal--badge'
})

function closeChatStory() {
  $('#chat-right').removeClass('is-show');
}

$('.chat__link').click(function(e) {
  e.preventDefault();
  $('.chat__link').removeClass('is-active');
  $(this).addClass('is-active');
  $('.chat__content').removeClass('is-active');
  $('.chat__content').eq($(this).index()).addClass('is-active');
});

$('.chat__blocked-close').click(function(e) {
  e.preventDefault();
  $('.chat__blocked').fadeOut('fast');
});
